# Installation de docker

[Installation de docker](https://docs.docker.com/engine/install/)
(Attention l'étape de vérification de l'installation a l'aide de l'image `hello-world` nécessitera peut-être de suivre l'article [Configuration du proxy](https://docs.docker.com/config/daemon/systemd/#httphttps-proxy) avant de poursuivre)

[Installation de docker-compose](https://docs.docker.com/compose/install/)

[Configuration du proxy](https://docs.docker.com/config/daemon/systemd/#httphttps-proxy)

[Lancement de docker sans sudo](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user)

## Version minimale
```shell script
$  docker-compose --version
docker-compose version 1.29.0, build 980ec85b
```

# Commandes de base

```shell script
# Vérification de la syntax
docker-compose config

# Lancement de docker compose / re-lancement si modifications détectées.
# Sudo si les "conf" redis n'ont pas leur droits "other" writable
sudo docker-compose --profile default up -d --build

# Arrêt de docker compose
docker-compose down --remove-orphans
```

# Lancement d'un profil

```shell script
# En commande line
$ docker-compose \
 --profile mysql \
 --profile mongo \
 --profile redis \
 --profile mq \
 --profile kafka \
 --profile mail \
 up -d

# Le profile `default` permet le lancement de tous les services
$ docker-compose --profile default up -d

# Avec une variable d'env
$ COMPOSE_PROFILES=mysql,mongo
$ docker-compose up -d
```

# Alias

Par simplicité, on peut ajouter des alias au chargement de profil pour piloter ces dockers de n'importe où.
> **Note :_** Mettez a jour la commande en fonction de l'endroit ou vous avez installé ce projet. les alias peuvent être mis dans votre .bashrc, .zshrc ou tout autre .xxxrc selon votre shell. Ou dans un autre fichier, tel que ~/.bash_aliases s'il est lu par votre .xxxrc

```
alias dck.up="sudo docker-compose --profile default --env-file ~/docker/.env -f ~/docker/docker-compose.yml up -d --build"
alias dck.fup="sudo docker-compose --profile default --env-file ~/docker/.env -f ~/docker/docker-compose.yml up -d --force-recreate --no-deps"
alias dck.down="docker-compose --profile default --env-file ~/docker/.env -f ~/docker/docker-compose.yml down --remove-orphans"
```

En utilisation, un `dck.up` ou un `dck.down` suffit, mais on peut préciser le service qui nous intéresse également : `dck.up mysql`. (Pour épargner sa machine si l'on a pas besoin de tous les services).
Un `dck.fup mysql` rejouera le script d'installation de la BDD.
 

# Liste des services

[kafka](src/service/kafka/README.md?ri)

[mail](src/service/mail/README.md?ri)

[mongo](src/service/mongo/README.md?ri)

[mq](src/service/mq/README.md?ri)

[mysql](src/service/mysql/README.md?ri)

[redis](src/service/redis/README.md?ri)

# Désinstallation

Ci-après, un script "bombe nucléaire", qui supprime tous les dockers / volumes de la machine

> **_Note :_** Attention, si vous avez d'autres dockers / volumes arrêtés sur votre machine, ils seront également supprimés. À utiliser a vos risques et périls.

```
docker stop $(docker ps -a -q)
docker rm -f $(docker ps -a -q)
docker volume rm $(docker volume ls -q)
docker network prune -f
```

# FAQ

Pourquoi quand je lance le docker-compose la syntaxe `extend` n'est pas reconnue ?

Car la version minimale de docker-compose est inférieur à `1.29.0`.

# Sources

- [Nexus Arkea](http://go/nexus-docker)
- [dockerhub](https://hub.docker.com)
