# Mq

## Utilisation

* `application.yml`
```yaml
mq:
    hostname: localhost
    port: 1414
    queueManager: QM1
    channel: MQTUXDEV.AGGREGAPI
    username: admin
    password: passw0rd
```

## Ajout d'une configuration pour une API
S'il n'existe pas encore, et s'il est requis, ajouter un nouveau fichier `nom-api.mqsc` dans le dossier `src/services/mq` dans lequel seront rangés toutes les configurations de channel et de queues pour cette API.

## Ajout d'un channel
Il faut ajouter une ligne dans le fichier `.mqsc` de l'API concernée.
```
DEFINE CHANNEL(NOM_CHANNEL) CHLTYPE(SVRCONN)
```

## Suppression d'un channel
Il faut ajouter une ligne dans le fichier `.mqsc` de l'API concernée.
```
`DELETE CHANNEL(NOM_CHANNEL)`
```
> **_Note :_** Même si la ligne d'un channel est supprimée du fichier, elle reste valide jusqu'à ce qu'elle soit supprimée via cette commande.

## Ajout d'une queue
Il faut ajouter une ligne dans le fichier `.mqsc` de l'API concernée.
```
DEFINE QL(NOM_QUEUE)
```

## Suppression d'une queue
Il faut ajouter une ligne dans le fichier `.mqsc` de l'API concernée.
```
DELETE QLOCAL (NOM_QUEUE) PURGE
```
> **_Note :_** Même si la ligne de définition d'une queue est supprimée du fichier, elle reste valide jusqu'à ce qu'elle soit supprimée via cette commande.

## FAQ
* Que faire si j'ai une erreur `CC=2;RC=2540;AMQ9204: Connexion à l'hôte 'HOST(PORT)' rejetée.` au lancement de l'API ?

Soit la configuration de l'API n'est pas correcte, soit la file MQ n'est pas démarrée, soit il y a une autre erreur indiquée après, telle que l'absence d'un canal.

* Que faire si j'ai une erreur `CC=2;RC=2540;AMQ9520: Canal non défini à distance. [3=NOM_CHANNEL]` au lancement de l'API ?

Il faut ajouter le channel `NOM_CHANNEL`, cf [Ajout d'un channel](?id=ajout-d39un-channel)

* Que faire si j'ai une erreur `JMSWMQ2008: Impossible d'ouvrir la file d'attente MQ NOM_QUEUE` au lancement de mon API ?

Il faut ajouter la queue NOM_QUEUE, cf [Ajout d'une queue](?id=ajout-d39une-queue)

## Sources

- [dockerhub](https://hub.docker.com/r/ibmcom/mq)
- [example](https://github.com/WASdev/sample.docker.mq)
- [queues](https://www.ibm.com/support/knowledgecenter/en/SSFKSJ_9.0.0/com.ibm.mq.adm.doc/q020710_.htm)
- [commands](https://www.ibm.com/support/knowledgecenter/SSFKSJ_9.1.0/com.ibm.mq.ref.adm.doc/q085820_.htm)
