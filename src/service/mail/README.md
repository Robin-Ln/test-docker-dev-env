# Mail

## Utilisation

* `application.yml`
```yaml
spring:
  mail:
    host: localhost
    port: 1025
    protocol: smtp
    defaultEncoding: UTF-8
```

* Exemples d'adresses mails
```yaml
mail:
    from: from@test.com
    to: to@test.com
    cc: cc@test.com
```
