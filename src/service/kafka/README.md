# Kafka

## Utilisation

* `application.yml`
```yaml
kafka:
    profile: DEV
    genericConfig:
        bootstrap.servers: localhost:9092
        group.id: agregation-api
```

## Sources

- [example](https://github.com/confluentinc/kafka-streams-examples/blob/5.5.0-post/docker-compose.yml)