# Redis

## Le cluster

Nous prévoyons dans le cluster un `master`, un `slave`, et un `sentinels`.

Nous devons au moins avoir un `master` et une `sentinel`. Car la configuration de nos APIs nous contraint à surcharger des sentinelles configurées le core / catalyst.

## Utilisation

Dans un le `application-<env>.yml` de votre projet :
```
spring:
    redis:
        sentinel:
            nodes: localhost:26379
            master: mymaster
```

## Pour vérifier votre installation

Un paquet est disponible sur aptitude pour tester redis
```
sudo apt-get install redis-tools
```

Vous pourrez vous connecter directement au master et aux répliques (si toujours actives).

```
➜ redis-cli -h localhost -p 6379
localhost:6379> get foo
(nil)
localhost:6379> set foo 123
OK
localhost:6379> get foo
"123"
```

## FAQ

- Pourquoi redis-sentinel reste à l'état `Restarting` ?

Selon votre gestion des droits (le fait que votre utilisateur linux soit dans le groupe docker, etc.), il est possible que les images des sentinelles redis ne démarrent pas.

```
➜ docker ps
... STATUS                        ...
    Restarting (0) 2 seconds ago  
```

Un `docker logs redis-sentinel-1` mettra en évidence que son fichier de configuration ne peut pas être réécrit :

```
1:X 29 Sep 2020 12:38:23.274 # Sentinel config file /etc/redis-sentinel-1.conf is not writable: Permission denied. Exiting...
```

Deux solutions, soit on lance les commandes `docker` en `sudo`, soit on change les droits en écriture des fichiers de configuration, pour qu'ils puissent être réécrits.
`chmod o+w src/service/redis/conf/*`


## Sources

- [Tuto](https://codingfullstack.com/java/spring-boot/spring-boot-redis-cluster/)
- [github](https://github.com/avinash10584/spring-boot-redis-cluster)
- [github](https://github.com/benweizhu/spring-redis-docker-example)