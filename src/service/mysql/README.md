# Mysql

## Utilisation

`application.yml`
```yaml
spring:
    datasource:
        url: jdbc:mysql://localhost:3306/[NOM_BDD_API]?serverTimezone=Europe/Paris
        username: root
        password: root
    jpa:
        properties:
            hibernate.hbm2ddl.auto: update
```
Remplacez `[NOM_BDD_API]` selon l'API.

## Ajout d'une configuration pour une API
Ajoutez une ligne de création de DB dans `init-db.sql`, puis recréez le docker mysql.
```
docker-compose -f src/env/dev/docker-compose.yml up -d --force-recreate --no-deps mysql
```

